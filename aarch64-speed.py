#!/usr/bin/env python3

import ctypes
import os
import subprocess
import time

SO_FILE = "/dev/shm/ins-test.so"

AFFINITIES = []
CPUS = []
CAL = []

TRIALS = 16
ITERS = 1000000

SYSFS_CPU = "/sys/bus/cpu/devices"

def detect_cpus():
    data = {}
    for subdir in os.listdir(SYSFS_CPU):
        num = int(subdir[3:])
        uevent = open(f"{SYSFS_CPU}/{subdir}/uevent", "r").readlines()
        typ = [l for l in uevent if l.startswith("OF_COMPATIBLE_0")][0]
        typ = typ.split(",")[1].strip()

        gov = open(f"{SYSFS_CPU}/{subdir}/cpufreq/scaling_governor").read()
        if gov.strip() != "performance":
            print(f"Warning: CPU {num} is not using the performance governor!")

        if typ in data:
            data[typ].append(num)
        else:
            data[typ] = [num]

    cpus = sorted(data)
    affinities = [data[c] for c in cpus]

    return cpus, affinities

def build_so(dest, text):
    subprocess.run(["gcc", "-shared", "-x", "assembler", "-o", dest, "-"],
                   input=bytes(text, encoding="utf8"))

def foreach_affinity(fn, *args, **kwargs):
    ret = []
    for a in AFFINITIES:
        os.sched_setaffinity(os.getpid(), a)
        ret.append(fn(*args, **kwargs))
    return ret

def dlclose(lib):
    c = ctypes.CDLL(None).dlclose
    c.argtypes = [ctypes.c_void_p]
    c(lib._handle)

so = None

def get_library(text):
    global so
    if so:
        dlclose(so)
    build_so(SO_FILE, text)
    so = ctypes.cdll.LoadLibrary(SO_FILE)
    return so

def iter_func(name, text):
    return f"""
	.global {name}
{name}:
	fmov v0.4s, 1.0
	fmov v1.4s, 1.5
	fmov v2.4s, 2.0
	fmov v3.4s, 2.5
	fmov v4.4s, 3.0
	fmov v5.4s, 3.5
	fmov v6.4s, 4.0
	mrs x2, CNTVCT_EL0
1:
	subs x0, x0, 1
	{text}
	b.cs 1b
	mrs x3, CNTVCT_EL0
	sub x0, x3, x2
	ret
    """

def test_function(l, fns, *args):
    res = [l[x](*args) for x in fns]
    return res

def test_set(l, instr, fns):
    test_function(l, fns, 10)
    r = [test_function(l, fns, ITERS) for x in range(TRIALS)]
    r = [sum(x) / len(x) / ITERS for x in zip(*r)]
    return r

def test_lib(l, instrs, fnss):
    return [test_set(l, instr, fns) for instr, fns in zip(instrs, fnss)]

def test_asm(instrs):
    asm_text = "\t.arch armv8-a\n\t.text\n"

    for r, instr in enumerate(instrs):
        for i in range(3, 9):
            asm_text += iter_func(f"t{r}_{i}", "\n\t".join([instr] * i))

    l = get_library(asm_text)

    return foreach_affinity(test_lib, l, instrs,
                            [[f"t{r}_{i}" for i in range(3, 9)]
                             for r in range(len(instrs))])

def callibrate():
    global CAL

    asm_text = "\t.arch armv8-a\n\t.text\n" + iter_func("t", "")
    l = get_library(asm_text)

    num = 5000000

    time.perf_counter_ns()
    l.t(0)
    before = time.perf_counter_ns()
    count = l.t(num)
    after = time.perf_counter_ns()

    ns = after - before

    cpu = os.sched_getaffinity(os.getpid()).pop()
    freq = int(open(f"{SYSFS_CPU}/cpu{cpu}/cpufreq/scaling_cur_freq", "r").read())

    clocks_per_iteration = round(freq * ns / num / 1000000 * 8) / 8

    return num / clocks_per_iteration / count

def calc_change(results, cal, name):
    cycles = [round(x * cal * 2) / 2 for x in results]

    diff = [y - x for y, x in zip(cycles[1:], cycles[:-1])]

    res = diff[0]

    for x in diff:
        if x != res:
            print(f"{name} may be incorrect", cycles)
            break

    return res

def fmt(num):
    if num == int(num):
        return int(num)
    else:
        return num

def test_instr_variant(instr):
    lat = instr.replace("{dest}", "1")
    thr = instr.replace("{dest}", "0")

    r = test_asm([lat, thr])

    lr = [calc_change(rr[0], cal, c + " latency")
          for rr, cal, c in zip(r, CAL, CPUS)]
    tr = [calc_change(rr[1], cal, c + " throughput")
          for rr, cal, c in zip(r, CAL, CPUS)]

    for l, t in zip(lr, tr):
        print(f"{fmt(l)}\t{fmt(t)}\t", end="")

    print(thr)

def test_instr(instr):
    LISTS = {
        "Ta": ("8B", "16B"),
        "Tf": ("2S", "4S", "2D"),
        "Tm": ("16B", "8H", "2S", "4S"),
        "Ts": ("2S", "4S"),
        "V": "B",

        "d": "{dest}",
        "Vd": "v<d>",
        "Vn": "v1",
        "Vn+1": "v2",
        "Vn+2": "v3",
        "Vn+3": "v4",
        "Vm": "v5",
    }

    s = sorted(list(set(x.split(">")[0] for x in instr.split("<")[1:])))[:1]

    if len(s):
        s = s[0]
        opts = LISTS[s]
        if isinstance(opts, str):
            opts = (opts,)
        for o in opts:
            test_instr(instr.replace(f"<{s}>", o))
    else:
        test_instr_variant(instr)

instrs = [
    "TBL <Vd>.<Ta>, { <Vn>.16B }, <Vm>.<Ta>",
    "TBL <Vd>.<Ta>, { <Vn>.16B, <Vn+1>.16B }, <Vm>.<Ta>",
    "TBL <Vd>.<Ta>, { <Vn>.16B, <Vn+1>.16B, <Vn+2>.16B, <Vn+3>.16B }, <Vm>.<Ta>",
    "ADDV <V><d>, <Vn>.<Ta>",
    "ADD <Vd>.<Ta>, <Vn>.<Ta>, <Vm>.<Ta>",
    "ADDP <Vd>.<Ta>, <Vn>.<Ta>, <Vm>.<Ta>",
    "MUL <Vd>.<Tm>, <Vn>.<Tm>, <Vm>.<Tm>",
    "PMUL <Vd>.<Ta>, <Vn>.<Ta>, <Vm>.<Ta>",

    "FADD <Vd>.<Tf>, <Vn>.<Tf>, <Vm>.<Tf>",
    "FMUL <Vd>.<Tf>, <Vn>.<Tf>, <Vm>.<Tf>",
    "FDIV <Vd>.<Tf>, <Vn>.<Tf>, <Vm>.<Tf>",
]

CPUS, AFFINITIES = detect_cpus()
CAL = foreach_affinity(callibrate)

print("Values are number of cycles")
print("For 'lat' (latency) instructions have a data dependency, "
      "but not for throughput")
print("\t".join(CPUS))
print("lat\tthr\t" * len(CPUS) + "ins")

for i in instrs:
    test_instr(i)
